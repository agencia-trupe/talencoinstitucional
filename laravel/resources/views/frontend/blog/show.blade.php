@extends('frontend.common.template')

@section('content')

    @include('frontend.blog._categorias')

    <div class="blog blog-show">
        <div class="capa">
            <div class="center">
                <div class="titulo-wrapper">
                    <div class="titulo">
                        <div>
                            <h1>{{ $post->titulo }}</h1>
                            <span>{{ $post->categoria->titulo }}</span>
                        </div>
                    </div>
                </div>

                <div class="imagem">
                    <img src="{{ Tools::blogAsset('assets/img/blog/destaque/'.$post->capa) }}" alt="">
                    <span>{{ Tools::formataData($post->data) }}</span>
                </div>
            </div>
        </div>

        <div class="center">
            <article>
                {!! $post->texto !!}

                <div class="comentarios">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.10&appId=1793598344250192";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-comments" data-href="{{ request()->url() }}" data-numposts="5" data-width="100%"></div>
                </div>
            </article>

            <aside>
                <form action="{{ route('blog') }}" method="GET" class="busca">
                    <input type="text" name="busca" placeholder="buscar" value="{{ request()->get('busca') }}" required>
                    <input type="submit" value="">
                </form>

                <nav>
                    @foreach($categorias as $categoria)
                    <a href="{{ route('blog', $categoria->slug) }}" @if($post->categoria == $categoria) class="active" @endif>{{ $categoria->titulo }}</a>
                    @endforeach
                </nav>

                <a href="{{ route('blog') }}" class="voltar">VOLTAR</a>
            </aside>
        </div>

        @if(count($leiaMais))
        <div class="leia-mais">
            <div class="center">
                @foreach($leiaMais as $post)
                <a href="{{ route('blog.show', [$post->categoria->slug, $post->slug]) }}">
                    <div class="imagem">
                        <img src="{{ Tools::blogAsset('assets/img/blog/mais/'.$post->capa) }}" alt="">
                        <span>{{ Tools::formataData($post->data) }}</span>
                    </div>

                    <h4>{{ $post->categoria->titulo }}</h4>
                    <h3>{{ $post->titulo }}</h3>
                    <p>{{ Tools::chamadaPost($post->texto) }}</p>

                    <span class="mais">LER MAIS</span>
                </a>
                @endforeach
            </div>
        </div>
        @endif
    </div>

@endsection
