@extends('frontend.common.template')

@section('content')

    @include('frontend.blog._categorias')

    <div class="blog blog-index">
        @if(count($posts) && !request()->get('busca'))
        <a href="{{ route('blog.show', [$posts->first()->categoria->slug, $posts->first()->slug]) }}" class="capa">
            <div class="center">
                <div class="titulo-wrapper">
                    <div class="titulo">
                        <div>
                            <h1>{{ $posts->first()->titulo }}</h1>
                            <span class="categoria">{{ $posts->first()->categoria->titulo }}</span>
                            <p>{{ Tools::chamadaPost($posts->first()->texto) }}</p>
                            <span class="mais">LER MAIS</span>
                        </div>
                    </div>
                </div>

                <div class="imagem">
                    <img src="{{ Tools::blogAsset('assets/img/blog/destaque/'.$posts->first()->capa) }}" alt="">
                    <span>{{ Tools::formataData($posts->first()->data) }}</span>
                </div>
            </div>
        </a>
        @endif

        <?php $limite = request()->get('busca') ? 3 : 4; ?>
        <div class="center">
            <div class="blog-posts-wrapper">
                <div class="blog-posts">
                @if(!count($posts))
                    <div class="nenhum">Nenhuma postagem encontrada.</div>
                @else
                    @foreach($posts as $key => $post)
                        @if(!$key == 0 || request()->get('busca'))
                            @if($key <= $limite)
                            <a href="{{ route('blog.show', [$post->categoria->slug, $post->slug]) }}" class="top4">
                                <div class="imagem-wrapper">
                                    <div class="imagem">
                                        <img src="{{ Tools::blogAsset('assets/img/blog/mais/'.$post->capa) }}" alt="">
                                        <span class="categoria">{{ $post->categoria->titulo }}</span>
                                        <span class="data">{{ Tools::formataData($post->data) }}</span>
                                    </div>
                                </div>
                                <div class="titulo">
                                    <h5>{{ $post->titulo }}</h5>
                                </div>
                            </a>
                            @else
                            @include('frontend.blog._thumb', ['post' => $post])
                            @endif
                        @endif
                    @endforeach
                @endif
                </div>

                @if ($posts->hasMorePages())
                <a href="#" class="blog-ver-mais" data-next="{{ route('blog', [null, 'page' => 2, 'busca' => request()->get('busca')]) }}">
                    <div>VER MAIS <span></span></div>
                </a>
                @endif
            </div>

            <aside>
                <form action="{{ route('blog') }}" method="GET" class="busca">
                    <input type="text" name="busca" placeholder="buscar" value="{{ request()->get('busca') }}" required>
                    <input type="submit" value="">
                </form>

                <nav>
                    @foreach($categorias as $cat)
                    <a href="{{ route('blog', $cat->slug) }}" @if(isset($categoria) && $categoria == $cat) class="active" @endif>{{ $cat->titulo }}</a>
                    @endforeach
                </nav>

                {{--<form action="" class="newsletter" id="form-newsletter">
                    <p>CADASTRE-SE PARA RECEBER NOVIDADES</p>
                    <input type="text" name="newsletter_nome" id="newsletter_nome" placeholder="nome" required>
                    <input type="email" name="newsletter_email" id="newsletter_email" placeholder="e-mail" required>
                    <button>OK <span></span></button>
                    <div id="form-newsletter-response"></div>
                </form>--}}
            </aside>
        </div>
    </div>

@endsection
