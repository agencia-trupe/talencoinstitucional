@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            <div class="center">
                @foreach($banners as $banner)
                <a href="{{ $banner->link }}">
                    <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="">
                    <div class="overlay"></div>
                    <div class="texto">
                        <h3>{{ $banner->{'titulo_'.app()->getLocale()} }}</h3>
                        <p>{{ $banner->{'texto_'.app()->getLocale()} }}</p>
                        <span>{{ trans('frontend.visitar') }}</span>
                    </div>
                </a>
                @endforeach
            </div>
        </div>

        <img src="{{ asset('assets/img/layout/slogan-'.app()->getLocale().'.png') }}" class="slogan" alt="">

        <div class="chamadas">
            <div class="center">
                <div class="chamada chamada-1">
                    <div class="imagem" style="background-image:url({{ asset('assets/img/chamadas/'.$chamadas->imagem_1) }})"></div>
                    <div class="texto">
                        <h2>{{ $chamadas->{'titulo_1_'.app()->getLocale()} }}</h2>
                    </div>
                </div>
                <a href="{{ $chamadas->link_2 }}" class="chamada chamada-2">
                    <div class="imagem" style="background-image:url({{ asset('assets/img/chamadas/'.$chamadas->imagem_2) }})"></div>
                    <div class="texto">
                        <h3>{{ $chamadas->{'titulo_2_'.app()->getLocale()} }}</h3>
                        <p>{{ $chamadas->{'texto_2_'.app()->getLocale()} }}</p>
                        <span>LEIA MAIS</span>
                    </div>
                </a>
                <a href="{{ $chamadas->link_3 }}" class="chamada chamada-3">
                    <div class="texto">
                        <h3>{{ $chamadas->{'titulo_3_'.app()->getLocale()} }}</h3>
                        <p>{{ $chamadas->{'texto_3_'.app()->getLocale()} }}</p>
                        <span>LEIA MAIS</span>
                    </div>
                    <div class="imagem" style="background-image:url({{ asset('assets/img/chamadas/'.$chamadas->imagem_3) }})"></div>
                </a>
                <a href="{{ $chamadas->link_4 }}" class="chamada chamada-4">
                    <div class="texto">
                        <h3>{{ $chamadas->{'titulo_4_'.app()->getLocale()} }}</h3>
                        <p>{{ $chamadas->{'texto_4_'.app()->getLocale()} }}</p>
                        <span>LEIA MAIS</span>
                    </div>
                    <div class="imagem" style="background-image:url({{ asset('assets/img/chamadas/'.$chamadas->imagem_4) }})"></div>
                </a>
                <a href="{{ $chamadas->link_5 }}" class="chamada chamada-5">
                    <div class="imagem" style="background-image:url({{ asset('assets/img/chamadas/'.$chamadas->imagem_5) }})"></div>
                    <div class="texto">
                        <h3>{{ $chamadas->{'titulo_5_'.app()->getLocale()} }}</h3>
                        <p>{{ $chamadas->{'texto_5_'.app()->getLocale()} }}</p>
                        <span>LEIA MAIS</span>
                    </div>
                </a>
                <a href="{{ $chamadas->link_6 }}" class="chamada chamada-6">
                    <div class="imagem" style="background-image:url({{ asset('assets/img/chamadas/'.$chamadas->imagem_6) }})"></div>
                    <div class="texto">
                        <h3>{{ $chamadas->{'titulo_6_'.app()->getLocale()} }}</h3>
                        <p>{{ $chamadas->{'texto_6_'.app()->getLocale()} }}</p>
                        <span>LEIA MAIS</span>
                    </div>
                </a>
            </div>
        </div>
    </div>

@endsection
