@extends('frontend.common.template')

@section('content')

    <div class="unidades">
        <div class="cabecalho" style="background-image:url({{ asset('assets/img/cabecalhos/'.$cabecalho) }})"></div>

        <div class="wrapper">
            <div class="center">
                <aside>
                    <h2>{{ trans('frontend.menu.unidades') }}</h2>
                    @foreach($links as $link)
                    <a href="{{ route('unidades', $link->slug) }}" @if($link->slug == $unidade->slug) class="active" @endif>
                        {{ $link->{'titulo_'.app()->getLocale()} }}
                    </a>
                    @endforeach
                </aside>

                <article>
                    <h1>{{ $unidade->{'titulo_'.app()->getLocale()} }}</h1>
                    {!! $unidade->{'texto_'.app()->getLocale()} !!}

                    @if(count($unidade->detalhes))
                    <div class="detalhes">
                        @foreach($unidade->detalhes as $detalhe)
                        <div class="detalhe">
                            <img src="{{ asset('assets/img/detalhes/'.$detalhe->imagem) }}" alt="">
                            <div class="texto">
                                <h3>{{ $detalhe->{'titulo_'.app()->getLocale()} }}</h3>
                                <p>{{ $detalhe->{'texto_'.app()->getLocale()} }}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </article>
            </div>
        </div>
    </div>

@endsection
