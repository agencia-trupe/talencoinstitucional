@extends('frontend.common.template')

@section('content')

    <div class="sobre">
        <div class="cabecalho" style="background-image:url({{ asset('assets/img/cabecalhos/'.$cabecalho) }})"></div>

        <div class="wrapper">
            <div class="center">
                <aside>
                    <h2>{{ trans('frontend.menu.sobre') }}</h2>
                    @foreach($links as $link)
                    <a href="{{ route('sobre', $link->slug) }}" @if($link->slug == $sobre->slug) class="active" @endif>
                        {{ $link->{'titulo_'.app()->getLocale()} }}
                    </a>
                    @endforeach
                </aside>

                <article>
                    <h1>{{ $sobre->{'titulo_'.app()->getLocale()} }}</h1>
                    @if($sobre->id == 2)
                    @foreach(['missao', 'visao', 'valores'] as $i)
                    <div class="box-wrapper">
                        <div class="titulo">{{ trans('frontend.sobre.'.$i) }}</div>
                        <div class="texto">{{ $sobre->{$i.'_'.app()->getLocale()} }}</div>
                    </div>
                    @endforeach
                    @else
                    {!! $sobre->{'texto_'.app()->getLocale()} !!}
                    @endif
                </article>
            </div>
        </div>
    </div>

@endsection
