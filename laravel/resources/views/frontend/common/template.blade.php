<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url('/') }}">

    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="{{ date('Y') }} Trupe Agência Criativa">
@if(isset($blogPost) && $blogPost->description)
    <meta name="description" content="{{ $blogPost->description }}">
@else
    <meta name="description" content="{{ $config->description }}">
@endif
@if(isset($blogPost) && $blogPost->keywords)
    <meta name="keywords" content="{{ $blogPost->keywords }}">
@else
    <meta name="keywords" content="{{ $config->keywords }}">
@endif

@if(isset($blogPost))
    <meta property="og:title" content="{{ $blogPost->titulo }} | {{ $config->title }}">
@else
    <meta property="og:title" content="{{ $config->title }}">
@endif
@if(isset($blogPost) && $blogPost->description)
    <meta property="og:description" content="{{ $blogPost->description }}">
@else
    <meta property="og:description" content="{{ $config->description }}">
@endif
    <meta property="og:site_name" content="{{ $config->title }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
@if(isset($blogPost))
    <meta property="og:image" content="{{ Tools::blogAsset('assets/img/blog/destaque/'.$blogPost->capa) }}">
@elseif($config->imagem_de_compartilhamento)
    <meta property="og:image" content="{{ asset('assets/img/'.$config->imagem_de_compartilhamento) }}">
@endif

@if(isset($blogPost))
    <title>{{ $blogPost->titulo }} | {{ $config->title }}</title>
@else
    <title>{{ $config->title }}</title>
@endif

    {!! Tools::loadCss('css/vendor.main.css') !!}
    {!! Tools::loadCss('css/main.css') !!}
</head>
<body>
    @include('frontend.common.header')
    @yield('content')
    @include('frontend.common.footer')

    {!! Tools::loadJquery() !!}
    {!! Tools::loadJs('js/vendor.main.js') !!}
    {!! Tools::loadJs('js/main.js') !!}

@if($config->analytics)
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', '{{ $config->analytics }}', 'auto');
        ga('send', 'pageview');
    </script>
@endif
</body>
</html>
