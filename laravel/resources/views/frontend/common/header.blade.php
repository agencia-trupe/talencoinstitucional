    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ $config->nome_do_site }}</a>

            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>

            <div class="lang">
                @foreach(['pt', 'en', 'es'] as $lang)
                    <a href="{{ route('lang', $lang) }}" class="lang-{{ $lang }} @if(app()->getLocale() == $lang) active @endif">{{ $lang }}</a>
                @endforeach
            </div>

            <nav id="nav-desktop">
                @include('frontend.common.nav')
            </nav>
        </div>
    </header>
    <nav id="nav-mobile">
        @include('frontend.common.nav')
    </nav>
