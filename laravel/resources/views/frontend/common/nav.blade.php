<a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>Home</a>
<a href="{{ route('sobre') }}" @if(Tools::isActive('sobre')) class="active" @endif>{{ trans('frontend.menu.sobre') }}</a>
<a href="{{ route('unidades') }}" @if(Tools::isActive('unidades')) class="active" @endif>{{ trans('frontend.menu.unidades') }}</a>
<a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>{{ trans('frontend.menu.contato') }}</a>
