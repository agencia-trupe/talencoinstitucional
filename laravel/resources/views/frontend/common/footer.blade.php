    <footer>
        <div class="superior">
            <div class="center">
                <div class="newsletter">
                    <p>{{ trans('frontend.newsletter.cadastre') }}</p>
                    <form action="" id="form-newsletter" method="POST">
                        <input type="text" name="newsletter_nome" id="newsletter_nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
                        <input type="email" name="newsletter_email" id="newsletter_email" placeholder="e-mail" required>
                        <button><span></span></button>
                    </form>
                </div>
                <div class="espaco-talenco">
                    <a href="{{ route('blog') }}">
                        <span>{{ trans('frontend.espaco-talenco.saiba') }}</span>
                        {!! trans('frontend.espaco-talenco.titulo') !!}
                    </a>
                </div>
            </div>
        </div>
        <div class="inferior">
            <div class="center">
                <nav>@include('frontend.common.nav')</nav>
                <p class="endereco">
                    {{ str_replace('<br />', ' – ', $contato->{'endereco_'.app()->getLocale()}) }}<br>
                    {{ trans('frontend.contato.central') }}:
                    <?php $telefone = explode(' ', $contato->{'telefone_'.app()->getLocale()}); ?>
                    {{ array_shift($telefone) }} <strong>{{ join($telefone, ' ') }}</strong>
                </p>
                <p class="copyright">
                    © {{ date('Y') }} {{ $config->nome_do_site }} - {{ trans('frontend.copyright.direitos') }}
                    <span>|</span>
                    <a href="http://www.trupe.net" target="_blank">{{ trans('frontend.copyright.criacao') }}</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>

