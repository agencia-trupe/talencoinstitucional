@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="informacoes">
            <div class="center">
                <p class="telefone">
                    {{ trans('frontend.contato.central') }}<br>
                    <strong>{{ $contato->{'telefone_'.app()->getLocale()} }}</strong>
                </p>
                <p class="endereco">{!! $contato->{'endereco_'.app()->getLocale()} !!}</p>
            </div>
        </div>

        <div class="formulario">
            <div class="center">
                <form action="" id="form-contato" method="POST">
                    <p>{{ trans('frontend.contato.fale') }}</p>
                    <input type="text" name="nome" id="nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
                    <input type="email" name="email" id="email" placeholder="e-mail" required>
                    <input type="text" name="telefone" id="telefone" placeholder="{{ trans('frontend.contato.telefone') }}">
                    <textarea name="mensagem" id="mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required></textarea>
                    <button>
                        {{ trans('frontend.contato.enviar') }}
                        <span></span>
                    </button>
                </form>
            </div>

            <div class="mapa">{!! $contato->google_maps !!}</div>
        </div>
    </div>

@endsection
