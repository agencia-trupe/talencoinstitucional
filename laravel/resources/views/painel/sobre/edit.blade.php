@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Sobre /</small> {{ $registro->titulo_pt }}</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.sobre.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.sobre.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
