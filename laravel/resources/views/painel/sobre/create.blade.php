@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Sobre /</small> Adicionar Sobre</h2>
    </legend>

    {!! Form::open(['route' => 'painel.sobre.store', 'files' => true]) !!}

        @include('painel.sobre.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
