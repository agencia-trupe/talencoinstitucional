@include('painel.common.flash')

@if(isset($registro) && $registro->id === 2)
<div class="form-group">
    {!! Form::label('titulo_pt', 'Título PT') !!}
    {!! Form::text('titulo_pt', null, ['class' => 'form-control', 'disabled' => true]) !!}
    {!! Form::hidden('titulo_pt', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('titulo_en', 'Título EN') !!}
    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('titulo_es', 'Título ES') !!}
    {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('missao_pt', 'Missão PT') !!}
            {!! Form::textarea('missao_pt', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('visao_pt', 'Visão PT') !!}
            {!! Form::textarea('visao_pt', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('valores_pt', 'Valores PT') !!}
            {!! Form::textarea('valores_pt', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('missao_en', 'Missão EN') !!}
            {!! Form::textarea('missao_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('visao_en', 'Visão EN') !!}
            {!! Form::textarea('visao_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('valores_en', 'Valores EN') !!}
            {!! Form::textarea('valores_en', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('missao_es', 'Missão ES') !!}
            {!! Form::textarea('missao_es', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('visao_es', 'Visão ES') !!}
            {!! Form::textarea('visao_es', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('valores_es', 'Valores ES') !!}
            {!! Form::textarea('valores_es', null, ['class' => 'form-control', 'style' => 'resize:none']) !!}
        </div>
    </div>
</div>
@else
<div class="form-group">
    {!! Form::label('titulo_pt', 'Título PT') !!}
    {!! Form::text('titulo_pt', null, ['class' => 'form-control', 'disabled' => true]) !!}
    {!! Form::hidden('titulo_pt', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('texto_pt', 'Texto PT') !!}
    {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'sobre']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('titulo_en', 'Título EN') !!}
    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('texto_en', 'Texto EN') !!}
    {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'sobre']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('titulo_es', 'Título ES') !!}
    {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('texto_es', 'Texto ES') !!}
    {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'sobre']) !!}
</div>
@endif

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.sobre.index') }}" class="btn btn-default btn-voltar">Voltar</a>
