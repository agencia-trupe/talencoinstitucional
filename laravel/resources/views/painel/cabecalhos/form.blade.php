@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('sobre', 'Sobre (1920x310px)') !!}
    <img src="{{ url('assets/img/cabecalhos/'.$registro->sobre) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('sobre', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('unidades', 'Unidades (1920x310px)') !!}
    <img src="{{ url('assets/img/cabecalhos/'.$registro->unidades) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('unidades', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
