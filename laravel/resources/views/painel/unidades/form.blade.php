@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo_pt', 'Título PT') !!}
    {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('texto_pt', 'Texto PT') !!}
    {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'sobre']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('titulo_en', 'Título EN') !!}
    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('texto_en', 'Texto EN') !!}
    {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'sobre']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('titulo_es', 'Título ES') !!}
    {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('texto_es', 'Texto ES') !!}
    {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'sobre']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.unidades.index') }}" class="btn btn-default btn-voltar">Voltar</a>
