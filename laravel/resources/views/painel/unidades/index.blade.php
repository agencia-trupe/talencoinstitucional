@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Unidades
        </h2>
    </legend>


    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="unidades">
        <thead>
            <tr>
                <th>Título PT</th>
                <th>Detalhes</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->titulo_pt }}</td>
                <td>
                    <a href="{{ route('painel.unidades.detalhes.index', $registro->id) }}" class="btn btn-info btn-sm">
                        Editar
                    </a>
                </td>
                <td class="crud-actions">
                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.unidades.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
