<ul class="nav navbar-nav">
    <li class="dropdown @if(str_is('painel.chamadas*', Route::currentRouteName()) || str_is('painel.banners*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Home
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.banners.index') }}">Banners</a>
            </li>
            <li @if(str_is('painel.chamadas*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.chamadas.index') }}">Chamadas</a>
            </li>
        </ul>
    </li>
    <li @if(str_is('painel.cabecalhos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.cabecalhos.index') }}">Cabeçalhos</a>
    </li>
    <li @if(str_is('painel.sobre*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.sobre.index') }}">Sobre</a>
    </li>
	<li @if(str_is('painel.unidades*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.unidades.index') }}">Unidades</a>
	</li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName()) || str_is('painel.newsletter*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
            <li class="divider"></li>
            <li @if(str_is('painel.newsletter*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.newsletter.index') }}">Newsletter</a>
            </li>
        </ul>
    </li>
</ul>
