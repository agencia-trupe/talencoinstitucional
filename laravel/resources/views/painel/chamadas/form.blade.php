@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_1', 'Imagem 1 (300x220px)') !!}
            <img src="{{ url('assets/img/chamadas/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_1_pt', 'Título 1 PT') !!}
            {!! Form::text('titulo_1_pt', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_1_en', 'Título 1 EN') !!}
            {!! Form::text('titulo_1_en', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_1_es', 'Título 1 ES') !!}
            {!! Form::text('titulo_1_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2 (300x220px)') !!}
            <img src="{{ url('assets/img/chamadas/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_2_pt', 'Título 2 PT') !!}
            {!! Form::text('titulo_2_pt', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_2_en', 'Título 2 EN') !!}
            {!! Form::text('titulo_2_en', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_2_es', 'Título 2 ES') !!}
            {!! Form::text('titulo_2_es', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_2_pt', 'Texto 2 PT') !!}
            {!! Form::text('texto_2_pt', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_2_en', 'Texto 2 EN') !!}
            {!! Form::text('texto_2_en', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_2_es', 'Texto 2 ES') !!}
            {!! Form::text('texto_2_es', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_2', 'Link 2') !!}
            {!! Form::text('link_2', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_3', 'Imagem 3 (300x220px)') !!}
            <img src="{{ url('assets/img/chamadas/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_3_pt', 'Título 3 PT') !!}
            {!! Form::text('titulo_3_pt', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_3_en', 'Título 3 EN') !!}
            {!! Form::text('titulo_3_en', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_3_es', 'Título 3 ES') !!}
            {!! Form::text('titulo_3_es', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_3_pt', 'Texto 3 PT') !!}
            {!! Form::text('texto_3_pt', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_3_en', 'Texto 3 EN') !!}
            {!! Form::text('texto_3_en', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_3_es', 'Texto 3 ES') !!}
            {!! Form::text('texto_3_es', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_3', 'Link 3') !!}
            {!! Form::text('link_3', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_4', 'Imagem 4 (300x220px)') !!}
            <img src="{{ url('assets/img/chamadas/'.$registro->imagem_4) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_4', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_4_pt', 'Título 4 PT') !!}
            {!! Form::text('titulo_4_pt', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_4_en', 'Título 4 EN') !!}
            {!! Form::text('titulo_4_en', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_4_es', 'Título 4 ES') !!}
            {!! Form::text('titulo_4_es', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_4_pt', 'Texto 4 PT') !!}
            {!! Form::text('texto_4_pt', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_4_en', 'Texto 4 EN') !!}
            {!! Form::text('texto_4_en', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_4_es', 'Texto 4 ES') !!}
            {!! Form::text('texto_4_es', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_4', 'Link 4') !!}
            {!! Form::text('link_4', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_5', 'Imagem 5 (300x220px)') !!}
            <img src="{{ url('assets/img/chamadas/'.$registro->imagem_5) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_5', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_5_pt', 'Título 5 PT') !!}
            {!! Form::text('titulo_5_pt', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_5_en', 'Título 5 EN') !!}
            {!! Form::text('titulo_5_en', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_5_es', 'Título 5 ES') !!}
            {!! Form::text('titulo_5_es', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_5_pt', 'Texto 5 PT') !!}
            {!! Form::text('texto_5_pt', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_5_en', 'Texto 5 EN') !!}
            {!! Form::text('texto_5_en', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_5_es', 'Texto 5 ES') !!}
            {!! Form::text('texto_5_es', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_5', 'Link 5') !!}
            {!! Form::text('link_5', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('imagem_6', 'Imagem 6 (300x220px)') !!}
            <img src="{{ url('assets/img/chamadas/'.$registro->imagem_6) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('imagem_6', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_6_pt', 'Título 6 PT') !!}
            {!! Form::text('titulo_6_pt', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_6_en', 'Título 6 EN') !!}
            {!! Form::text('titulo_6_en', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('titulo_6_es', 'Título 6 ES') !!}
            {!! Form::text('titulo_6_es', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_6_pt', 'Texto 6 PT') !!}
            {!! Form::text('texto_6_pt', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_6_en', 'Texto 6 EN') !!}
            {!! Form::text('texto_6_en', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('texto_6_es', 'Texto 6 ES') !!}
            {!! Form::text('texto_6_es', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('link_6', 'Link 6') !!}
            {!! Form::text('link_6', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
