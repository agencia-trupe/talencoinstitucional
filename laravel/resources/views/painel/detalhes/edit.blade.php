@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>{{ $unidade->titulo_pt }} /</small> Editar Detalhe</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.unidades.detalhes.update', $unidade->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.detalhes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
