@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>{{ $unidade->titulo_pt }} /</small> Adicionar Detalhe</h2>
    </legend>

    {!! Form::open(['route' => ['painel.unidades.detalhes.store', $unidade->id], 'files' => true]) !!}

        @include('painel.detalhes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
