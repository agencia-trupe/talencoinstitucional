<?php

return [

    'menu' => [
        'sobre'    => 'About us',
        'unidades' => 'Business units',
        'contato'  => 'Contact',
    ],

    'visitar' => 'GO TO WEBSITE',

    'blog'          => 'BLOG',
    'blog-destaque' => '<strong>BLOG</strong>',

    'sobre' => [
        'missao'  => 'Mission',
        'visao'   => 'Vision',
        'valores' => 'Values',
    ],

    'contato' => [
        'central'  => 'Customer service',
        'fale'     => 'Contact',
        'nome'     => 'Name',
        'telefone' => 'phone',
        'mensagem' => 'message',
        'enviar'   => 'SEND',
        'sucesso'  => 'Message sent successfully!',
        'erro'     => 'Fullfill all the fields correctly.',
    ],

    'newsletter' => [
        'cadastre'       => 'Register to receive the news:',
        'nome-required'  => 'fullfill with your name',
        'email-required' => 'fullfill with an e-mail',
        'email-email'    => 'fullfill with a valid e-mail',
        'email-unique'   => 'the informed e-mail is already registered',
        'sucesso'        => 'registration successfully completed!',
    ],

    'espaco-talenco' => [
        'saiba'  => 'FIND MORE ON:',
        'titulo' => '<strong>BLOG</strong>',
    ],

    'copyright' => [
        'direitos' => 'All rights reserved',
        'criacao'  => 'Sites'
    ],

    '404' => 'Page not found',

];
