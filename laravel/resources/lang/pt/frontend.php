<?php

return [

    'menu' => [
        'sobre'    => 'Sobre a TalenCo',
        'unidades' => 'Unidades de Negócios',
        'contato'  => 'Contato',
    ],

    'visitar' => 'VISITAR O SITE',

    'blog'          => 'ESPAÇO TALENCO',
    'blog-destaque' => '<strong>ESPAÇO</strong>TALENCO',

    'sobre' => [
        'missao'  => 'Missão',
        'visao'   => 'Visão',
        'valores' => 'Valores',
    ],

    'contato' => [
        'central'  => 'Central de Relacionamento',
        'fale'     => 'Fale Conosco',
        'nome'     => 'nome',
        'telefone' => 'telefone',
        'mensagem' => 'mensagem',
        'enviar'   => 'ENVIAR',
        'sucesso'  => 'Mensagem enviada com sucesso!',
        'erro'     => 'Preencha todos os campos corretamente.',
    ],

    'newsletter' => [
        'cadastre'       => 'Cadastre-se para receber novidades:',
        'nome-required'  => 'preencha seu nome',
        'email-required' => 'insira um endereço de e-mail',
        'email-email'    => 'insira um endereço de e-mail válido',
        'email-unique'   => 'o e-mail inserido já está cadastrado',
        'sucesso'        => 'cadastro efetuado com sucesso!',
    ],

    'espaco-talenco' => [
        'saiba'  => 'SAIBA MAIS NO:',
        'titulo' => '<strong>ESPAÇO</strong>TALENCO',
    ],

    'copyright' => [
        'direitos' => 'Todos os direitos reservados',
        'criacao'  => 'Criação de sites'
    ],

    '404' => 'Página não encontrada',

];
