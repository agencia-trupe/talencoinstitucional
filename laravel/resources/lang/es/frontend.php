<?php

return [

    'menu' => [
        'sobre'    => 'Sobre TalenCo',
        'unidades' => 'Unidades de negocios',
        'contato'  => 'Contacto',
    ],

    'visitar' => 'VISITAR EL SITE',

    'blog'          => 'ESPACIO TALENCO',
    'blog-destaque' => '<strong>ESPACIO</strong>TALENCO',

    'sobre' => [
        'missao'  => 'Misión',
        'visao'   => 'Visión',
        'valores' => 'Valores',
    ],

    'contato' => [
        'central'  => 'Central de Relacionamientos',
        'fale'     => 'Hable com nosostros',
        'nome'     => 'nombe',
        'telefone' => 'teléfono',
        'mensagem' => 'mensaje',
        'enviar'   => 'ENVIAR',
        'sucesso'  => '¡Mensaje enviado con suceso!',
        'erro'     => 'Llene todos los campos correctamente.',
    ],

    'newsletter' => [
        'cadastre'       => 'Regístrese para recibir novedades:',
        'nome-required'  => 'llene su nombre',
        'email-required' => 'introduzca una dirección de e-mail',
        'email-email'    => 'introduzca una dirección de e-mail válido',
        'email-unique'   => 'el e-mail introducido ya está registrado',
        'sucesso'        => '¡registro efectuado con suceso!',
    ],

    'espaco-talenco' => [
        'saiba'  => 'SEPA MÁS EN EL:',
        'titulo' => '<strong>ESPACIO</strong>TALENCO',
    ],

    'copyright' => [
        'direitos' => 'Todos los derechos reservados',
        'criacao'  => 'Sitios'
    ],

    '404' => 'Página no encontrada',

];
