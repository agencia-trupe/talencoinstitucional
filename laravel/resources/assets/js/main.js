import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';
import Newsletter from './Newsletter';
import Contato from './Contato';

AjaxSetup();
MobileToggle();
Newsletter();
Contato();

$('.blog-ver-mais').click(function(event) {
    event.preventDefault();

    var $btn       = $(this),
        $container = $('.blog-posts');

    if ($btn.hasClass('loading')) return false;

    $btn.addClass('loading');

    $.get($btn.data('next'), function(data) {
        var posts = $(data.posts).hide();
        $container.append(posts);
        posts.fadeIn();

        $btn.removeClass('loading');

        if (data.nextPage) {
            $btn.data('next', data.nextPage);
        } else {
            $btn.fadeOut(function() {
                $this.remove();
            });
        }
    });
});
