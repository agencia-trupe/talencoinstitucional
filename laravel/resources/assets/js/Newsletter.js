export default function Newsletter() {
    var envioNewsletter = function(event) {
        event.preventDefault();

        var $form = $(this);

        if ($form.hasClass('sending')) return false;

        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/newsletter',
            data: {
                nome: $('#newsletter_nome').val(),
                email: $('#newsletter_email').val(),
            },
            success: function(data) {
                alert(data.message);
                $form[0].reset();
            },
            error: function(data) {
                var errorMsg = data.responseJSON.nome ? data.responseJSON.nome : data.responseJSON.email;
                alert(errorMsg);
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    $('#form-newsletter').on('submit', envioNewsletter);
};
