<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('sobre-a-talenco/{sobre_slug?}', 'SobreController@index')->name('sobre');
    Route::get('unidades-de-negocios/{unidade_slug?}', 'UnidadesController@index')->name('unidades');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::post('newsletter', 'HomeController@newsletter')->name('newsletter');
    Route::get('espaco-talenco/{categoria_slug?}', 'BlogController@index')->name('blog');
    Route::get('espaco-talenco/{categoria_slug}/{post_slug}', 'BlogController@show')->name('blog.show');

    // Localização
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if ($idioma == 'pt' || $idioma == 'en' || $idioma == 'es') {
            Session::put('locale', $idioma);
        }
        return redirect()->route('home');
    })->name('lang');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::resource('unidades', 'UnidadesController', ['only' => ['index', 'edit', 'update']]);
		Route::resource('unidades.detalhes', 'DetalhesController');
		Route::resource('chamadas', 'ChamadasController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController', ['only' => ['index', 'edit', 'update']]);
		Route::resource('cabecalhos', 'CabecalhosController', ['only' => ['index', 'update']]);
		Route::resource('sobre', 'SobreController', ['only' => ['index', 'edit', 'update']]);
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::get('newsletter/exportar', ['as' => 'painel.newsletter.exportar', 'uses' => 'NewsletterController@exportar']);
        Route::resource('newsletter', 'NewsletterController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
