<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SobreRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'titulo_es' => 'required',
            'texto_pt' => 'required',
            'texto_en' => 'required',
            'texto_es' => 'required',
        ];

        $rulesMissao = [
            'missao_pt'  => 'required',
            'visao_pt'   => 'required',
            'valores_pt' => 'required',
            'missao_en'  => 'required',
            'visao_en'   => 'required',
            'valores_en' => 'required',
            'missao_es'  => 'required',
            'visao_es'   => 'required',
            'valores_es' => 'required',
        ];

        if ($this->method() != 'POST') {
            if ($this->sobre->id === 2) return $rulesMissao;
        }

        return $rules;
    }
}
