<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ChamadasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem_1' => 'image',
            'titulo_1_pt' => 'required',
            'titulo_1_en' => 'required',
            'titulo_1_es' => 'required',
            // 'link_1' => 'required',
            'imagem_2' => 'image',
            'titulo_2_pt' => 'required',
            'titulo_2_en' => 'required',
            'titulo_2_es' => 'required',
            'texto_2_pt' => 'required',
            'texto_2_en' => 'required',
            'texto_2_es' => 'required',
            'link_2' => 'required',
            'imagem_3' => 'image',
            'titulo_3_pt' => 'required',
            'titulo_3_en' => 'required',
            'titulo_3_es' => 'required',
            'texto_3_pt' => 'required',
            'texto_3_en' => 'required',
            'texto_3_es' => 'required',
            'link_3' => 'required',
            'imagem_4' => 'image',
            'titulo_4_pt' => 'required',
            'titulo_4_en' => 'required',
            'titulo_4_es' => 'required',
            'texto_4_pt' => 'required',
            'texto_4_en' => 'required',
            'texto_4_es' => 'required',
            'link_4' => 'required',
            'imagem_5' => 'image',
            'titulo_5_pt' => 'required',
            'titulo_5_en' => 'required',
            'titulo_5_es' => 'required',
            'texto_5_pt' => 'required',
            'texto_5_en' => 'required',
            'texto_5_es' => 'required',
            'link_5' => 'required',
            'imagem_6' => 'image',
            'titulo_6_pt' => 'required',
            'titulo_6_en' => 'required',
            'titulo_6_es' => 'required',
            'texto_6_pt' => 'required',
            'texto_6_en' => 'required',
            'texto_6_es' => 'required',
            'link_6' => 'required',
        ];
    }
}
