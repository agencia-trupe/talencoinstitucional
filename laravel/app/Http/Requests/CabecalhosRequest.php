<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CabecalhosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'sobre' => 'image',
            'unidades' => 'image',
        ];
    }
}
