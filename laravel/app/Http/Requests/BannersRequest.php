<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannersRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem' => 'required|image',
            'titulo_pt' => 'required',
            'texto_pt' => 'required',
            'titulo_en' => 'required',
            'texto_en' => 'required',
            'titulo_es' => 'required',
            'texto_es' => 'required',
            'link' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
