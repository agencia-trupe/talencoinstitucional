<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Unidade;
use App\Models\Cabecalhos;

class UnidadesController extends Controller
{
    public function index(Unidade $unidade)
    {
        if (!$unidade->exists) {
            $unidade = Unidade::ordenados()->first();
        }

        $links     = Unidade::ordenados()->get();
        $cabecalho = Cabecalhos::first()->unidades;

        return view('frontend.unidades', compact('links', 'unidade', 'cabecalho'));
    }
}
