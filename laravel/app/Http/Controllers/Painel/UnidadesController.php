<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\UnidadesRequest;
use App\Http\Controllers\Controller;

use App\Models\Unidade;

class UnidadesController extends Controller
{
    public function index()
    {
        $registros = Unidade::ordenados()->get();

        return view('painel.unidades.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.unidades.create');
    }

    public function store(UnidadesRequest $request)
    {
        try {

            $input = $request->all();

            Unidade::create($input);

            return redirect()->route('painel.unidades.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Unidade $registro)
    {
        return view('painel.unidades.edit', compact('registro'));
    }

    public function update(UnidadesRequest $request, Unidade $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.unidades.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Unidade $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.unidades.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
