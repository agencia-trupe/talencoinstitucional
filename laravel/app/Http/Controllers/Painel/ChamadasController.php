<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ChamadasRequest;
use App\Http\Controllers\Controller;

use App\Models\Chamadas;

class ChamadasController extends Controller
{
    public function index()
    {
        $registro = Chamadas::first();

        return view('painel.chamadas.edit', compact('registro'));
    }

    public function update(ChamadasRequest $request, Chamadas $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = Chamadas::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = Chamadas::upload_imagem_2();
            if (isset($input['imagem_3'])) $input['imagem_3'] = Chamadas::upload_imagem_3();
            if (isset($input['imagem_4'])) $input['imagem_4'] = Chamadas::upload_imagem_4();
            if (isset($input['imagem_5'])) $input['imagem_5'] = Chamadas::upload_imagem_5();
            if (isset($input['imagem_6'])) $input['imagem_6'] = Chamadas::upload_imagem_6();

            $registro->update($input);

            return redirect()->route('painel.chamadas.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
