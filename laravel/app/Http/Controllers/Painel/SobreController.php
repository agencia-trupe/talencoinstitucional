<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SobreRequest;
use App\Http\Controllers\Controller;

use App\Models\Sobre;

class SobreController extends Controller
{
    public function index()
    {
        $registros = Sobre::ordenados()->get();

        return view('painel.sobre.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.sobre.create');
    }

    public function store(SobreRequest $request)
    {
        try {

            $input = $request->all();

            Sobre::create($input);

            return redirect()->route('painel.sobre.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Sobre $registro)
    {
        return view('painel.sobre.edit', compact('registro'));
    }

    public function update(SobreRequest $request, Sobre $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.sobre.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Sobre $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.sobre.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
