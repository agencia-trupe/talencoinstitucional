<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CabecalhosRequest;
use App\Http\Controllers\Controller;

use App\Models\Cabecalhos;

class CabecalhosController extends Controller
{
    public function index()
    {
        $registro = Cabecalhos::first();

        return view('painel.cabecalhos.edit', compact('registro'));
    }

    public function update(CabecalhosRequest $request, Cabecalhos $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['sobre'])) $input['sobre'] = Cabecalhos::upload_sobre();
            if (isset($input['unidades'])) $input['unidades'] = Cabecalhos::upload_unidades();

            $registro->update($input);

            return redirect()->route('painel.cabecalhos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
