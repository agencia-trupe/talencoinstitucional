<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DetalhesRequest;
use App\Http\Controllers\Controller;

use App\Models\Unidade;
use App\Models\Detalhe;

class DetalhesController extends Controller
{
    public function index(Unidade $unidade)
    {
        $registros = Detalhe::where('unidade_id', $unidade->id)->ordenados()->get();

        return view('painel.detalhes.index', compact('unidade', 'registros'));
    }

    public function create(Unidade $unidade)
    {
        return view('painel.detalhes.create', compact('unidade'));
    }

    public function store(Unidade $unidade, DetalhesRequest $request)
    {
        try {

            $input = $request->all();
            $input['unidade_id'] = $unidade->id;

            if (isset($input['imagem'])) $input['imagem'] = Detalhe::upload_imagem();

            Detalhe::create($input);

            return redirect()->route('painel.unidades.detalhes.index', $unidade->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Unidade $unidade, Detalhe $registro)
    {
        return view('painel.detalhes.edit', compact('registro', 'unidade'));
    }

    public function update(Unidade $unidade, DetalhesRequest $request, Detalhe $registro)
    {
        try {

            $input = $request->all();
            $input['unidade_id'] = $unidade->id;

            if (isset($input['imagem'])) $input['imagem'] = Detalhe::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.unidades.detalhes.index', $unidade->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Unidade $unidade, Detalhe $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.unidades.detalhes.index', $unidade->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
