<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Sobre;
use App\Models\Cabecalhos;

class SobreController extends Controller
{
    public function index(Sobre $sobre)
    {
        if (!$sobre->exists) {
            $sobre = Sobre::ordenados()->first();
        }

        $links     = Sobre::ordenados()->get();
        $cabecalho = Cabecalhos::first()->sobre;

        return view('frontend.sobre', compact('links', 'sobre', 'cabecalho'));
    }
}
