<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\ContatoRecebido;
use App\Models\Contato;

class ContatoController extends Controller
{
    public function index()
    {
        $contato = Contato::first();

        return view('frontend.contato', compact('contato'));
    }

    public function post(ContatoRecebido $contatoRecebido, Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'nome'     => 'required',
            'email'    => 'required|email',
            'mensagem' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'  => 'error',
                'message' => trans('frontend.contato.erro')
            ]);
        }

        $contatoRecebido->create($request->all());

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[CONTATO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.contato.sucesso')
        ];

        return response()->json($response);
    }
}
