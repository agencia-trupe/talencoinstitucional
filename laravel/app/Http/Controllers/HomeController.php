<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\NewsletterRequest;
use App\Models\Newsletter;

use App\Models\Banner;
use App\Models\Chamadas;

class HomeController extends Controller
{
    public function index()
    {
        $banners  = Banner::ordenados()->get();
        $chamadas = Chamadas::first();

        return view('frontend.home', compact('banners', 'chamadas'));
    }

    public function newsletter(NewsletterRequest $request, Newsletter $newsletter)
    {
        $newsletter->create($request->all());

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.newsletter.sucesso')
        ];

        return response()->json($response);
    }
}
