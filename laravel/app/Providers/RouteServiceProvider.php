<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('detalhes', 'App\Models\Detalhe');
		$router->model('unidades', 'App\Models\Unidade');
		$router->model('chamadas', 'App\Models\Chamadas');
		$router->model('banners', 'App\Models\Banner');
		$router->model('cabecalhos', 'App\Models\Cabecalhos');
		$router->model('sobre', 'App\Models\Sobre');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('newsletter', 'App\Models\Newsletter');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('sobre_slug', function($value) {
            return \App\Models\Sobre::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('unidade_slug', function($value) {
            return \App\Models\Unidade::whereSlug($value)->first() ?: abort('404');
        });

        $router->bind('categoria_slug', function($value) {
            return \App\Models\PostCategoria::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('post_slug', function($value) {
            return \App\Models\Post::whereSlug($value)->first() ?: abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
