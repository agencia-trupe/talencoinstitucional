<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Cabecalhos extends Model
{
    protected $table = 'cabecalhos';

    protected $guarded = ['id'];

    public static function upload_sobre()
    {
        return CropImage::make('sobre', [
            'width'  => 1920,
            'height' => 310,
            'path'   => 'assets/img/cabecalhos/'
        ]);
    }

    public static function upload_unidades()
    {
        return CropImage::make('unidades', [
            'width'  => 1920,
            'height' => 310,
            'path'   => 'assets/img/cabecalhos/'
        ]);
    }

}
