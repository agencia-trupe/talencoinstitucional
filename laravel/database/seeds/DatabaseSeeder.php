<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
		$this->call(ChamadasSeeder::class);
		$this->call(CabecalhosSeeder::class);
		$this->call(ConfiguracoesSeeder::class);
        $this->call(ContatoTableSeeder::class);
        $this->call(SobreTableSeeder::class);
        $this->call(BannersTableSeeder::class);
        $this->call(UnidadesTableSeeder::class);

        Model::reguard();
    }
}
