<?php

use Illuminate\Database\Seeder;

class UnidadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('unidades')->insert([
            [
                'titulo_pt' => 'Construção E Incorporação',
                'slug'      => 'construcao-e-incorporacao',
                'ordem'     => 0
            ],
            [
                'titulo_pt' => 'Soluções Em Reformas',
                'slug'      => 'solucoes-em-reformas',
                'ordem'     => 1
            ],
            [
                'titulo_pt' => 'Negócios Imobiliários',
                'slug'      => 'negocios-imobiliarios',
                'ordem'     => 2
            ]
        ]);
    }
}
