<?php

use Illuminate\Database\Seeder;

class ChamadasSeeder extends Seeder
{
    public function run()
    {
        DB::table('chamadas')->insert([
            'imagem_1' => '',
            'titulo_1_pt' => '',
            'titulo_1_en' => '',
            'titulo_1_es' => '',
            'link_1' => '',
            'imagem_2' => '',
            'titulo_2_pt' => '',
            'titulo_2_en' => '',
            'titulo_2_es' => '',
            'texto_2_pt' => '',
            'texto_2_en' => '',
            'texto_2_es' => '',
            'link_2' => '',
            'imagem_3' => '',
            'titulo_3_pt' => '',
            'titulo_3_en' => '',
            'titulo_3_es' => '',
            'texto_3_pt' => '',
            'texto_3_en' => '',
            'texto_3_es' => '',
            'link_3' => '',
            'imagem_4' => '',
            'titulo_4_pt' => '',
            'titulo_4_en' => '',
            'titulo_4_es' => '',
            'texto_4_pt' => '',
            'texto_4_en' => '',
            'texto_4_es' => '',
            'link_4' => '',
            'imagem_5' => '',
            'titulo_5_pt' => '',
            'titulo_5_en' => '',
            'titulo_5_es' => '',
            'texto_5_pt' => '',
            'texto_5_en' => '',
            'texto_5_es' => '',
            'link_5' => '',
            'imagem_6' => '',
            'titulo_6_pt' => '',
            'titulo_6_en' => '',
            'titulo_6_es' => '',
            'texto_6_pt' => '',
            'texto_6_en' => '',
            'texto_6_es' => '',
            'link_6' => '',
        ]);
    }
}
