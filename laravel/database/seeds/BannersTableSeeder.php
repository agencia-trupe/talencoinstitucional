<?php

use Illuminate\Database\Seeder;

class BannersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banners')->insert([
            ['titulo_pt' => 'Construção & Incorporação', 'ordem' => 0],
            ['titulo_pt' => 'Soluções Em Reformas', 'ordem' => 1],
            ['titulo_pt' => 'Negócios Imobiliários', 'ordem' => 2],
        ]);
    }
}
