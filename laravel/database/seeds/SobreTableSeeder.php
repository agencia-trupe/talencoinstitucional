<?php

use Illuminate\Database\Seeder;

class SobreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sobre')->insert([
            [
                'titulo_pt' => 'Perfil Corporativo',
                'slug'      => 'perfil-corporativo',
                'ordem'     => 0
            ],
            [
                'titulo_pt' => 'Missão, Visão e Valores',
                'slug'      => 'missao-visao-e-valores',
                'ordem'     => 1
            ],
            [
                'titulo_pt' => 'Cultura Organizacional',
                'slug'      => 'cultura-organizacional',
                'ordem'     => 2
            ],
            [
                'titulo_pt' => 'Responsabilidade Social',
                'slug'      => 'responsabilidade-social',
                'ordem'     => 3
            ],
        ]);
    }
}
