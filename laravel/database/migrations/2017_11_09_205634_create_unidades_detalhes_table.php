<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadesDetalhesTable extends Migration
{
    public function up()
    {
        Schema::create('unidades_detalhes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unidade_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('titulo_pt');
            $table->text('texto_pt');
            $table->string('titulo_en');
            $table->text('texto_en');
            $table->string('titulo_es');
            $table->text('texto_es');
            $table->timestamps();
            $table->foreign('unidade_id')->references('id')->on('unidades')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('unidades_detalhes');
    }
}
