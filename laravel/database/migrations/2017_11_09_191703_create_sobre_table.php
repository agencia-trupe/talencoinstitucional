<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSobreTable extends Migration
{
    public function up()
    {
        Schema::create('sobre', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->text('missao_pt');
            $table->text('missao_en');
            $table->text('missao_es');
            $table->text('visao_pt');
            $table->text('visao_en');
            $table->text('visao_es');
            $table->text('valores_pt');
            $table->text('valores_en');
            $table->text('valores_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('sobre');
    }
}
