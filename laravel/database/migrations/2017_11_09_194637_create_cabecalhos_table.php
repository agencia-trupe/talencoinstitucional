<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCabecalhosTable extends Migration
{
    public function up()
    {
        Schema::create('cabecalhos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sobre');
            $table->string('unidades');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('cabecalhos');
    }
}
