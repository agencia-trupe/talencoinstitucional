<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChamadasTable extends Migration
{
    public function up()
    {
        Schema::create('chamadas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem_1');
            $table->string('titulo_1_pt');
            $table->string('titulo_1_en');
            $table->string('titulo_1_es');
            $table->string('link_1');
            $table->string('imagem_2');
            $table->string('titulo_2_pt');
            $table->string('titulo_2_en');
            $table->string('titulo_2_es');
            $table->text('texto_2_pt');
            $table->text('texto_2_en');
            $table->text('texto_2_es');
            $table->string('link_2');
            $table->string('imagem_3');
            $table->string('titulo_3_pt');
            $table->string('titulo_3_en');
            $table->string('titulo_3_es');
            $table->text('texto_3_pt');
            $table->text('texto_3_en');
            $table->text('texto_3_es');
            $table->string('link_3');
            $table->string('imagem_4');
            $table->string('titulo_4_pt');
            $table->string('titulo_4_en');
            $table->string('titulo_4_es');
            $table->text('texto_4_pt');
            $table->text('texto_4_en');
            $table->text('texto_4_es');
            $table->string('link_4');
            $table->string('imagem_5');
            $table->string('titulo_5_pt');
            $table->string('titulo_5_en');
            $table->string('titulo_5_es');
            $table->text('texto_5_pt');
            $table->text('texto_5_en');
            $table->text('texto_5_es');
            $table->string('link_5');
            $table->string('imagem_6');
            $table->string('titulo_6_pt');
            $table->string('titulo_6_en');
            $table->string('titulo_6_es');
            $table->text('texto_6_pt');
            $table->text('texto_6_en');
            $table->text('texto_6_es');
            $table->string('link_6');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('chamadas');
    }
}
