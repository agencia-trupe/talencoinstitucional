-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Tempo de geração: 10/11/2017 às 16:32
-- Versão do servidor: 8.0.2-dmr
-- Versão do PHP: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `talencoinstitucional`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_es` text COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `titulo_pt`, `texto_pt`, `titulo_en`, `texto_en`, `titulo_es`, `texto_es`, `link`, `created_at`, `updated_at`) VALUES
(1, 0, 'imghome-construcao_20171110162857.jpg', 'Construção & Incorporação', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '.', '.', '.', '.', '#', NULL, '2017-11-10 16:32:05'),
(2, 1, 'imghome-reformas_20171110162929.jpg', 'Soluções Em Reformas', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '.', '.', '.', '.', '#', NULL, '2017-11-10 16:32:09'),
(3, 2, 'imghome-negociosimob_20171110162938.jpg', 'Negócios Imobiliários', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '.', '.', '.', '.', '#', NULL, '2017-11-10 16:32:12');

-- --------------------------------------------------------

--
-- Estrutura para tabela `cabecalhos`
--

CREATE TABLE `cabecalhos` (
  `id` int(10) UNSIGNED NOT NULL,
  `sobre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unidades` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `cabecalhos`
--

INSERT INTO `cabecalhos` (`id`, `sobre`, `unidades`, `created_at`, `updated_at`) VALUES
(1, '8_20171110162305.jpg', '54_20171110162305.jpg', NULL, '2017-11-10 16:23:05');

-- --------------------------------------------------------

--
-- Estrutura para tabela `chamadas`
--

CREATE TABLE `chamadas` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_1_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_1_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_1_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_2_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_2_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_2_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_es` text COLLATE utf8_unicode_ci NOT NULL,
  `link_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_3_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_3_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_3_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_3_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_3_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_3_es` text COLLATE utf8_unicode_ci NOT NULL,
  `link_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_4_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_4_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_4_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_4_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_4_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_4_es` text COLLATE utf8_unicode_ci NOT NULL,
  `link_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_5_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_5_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_5_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_5_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_5_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_5_es` text COLLATE utf8_unicode_ci NOT NULL,
  `link_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_6_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_6_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_6_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_6_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_6_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_6_es` text COLLATE utf8_unicode_ci NOT NULL,
  `link_6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `chamadas`
--

INSERT INTO `chamadas` (`id`, `imagem_1`, `titulo_1_pt`, `titulo_1_en`, `titulo_1_es`, `link_1`, `imagem_2`, `titulo_2_pt`, `titulo_2_en`, `titulo_2_es`, `texto_2_pt`, `texto_2_en`, `texto_2_es`, `link_2`, `imagem_3`, `titulo_3_pt`, `titulo_3_en`, `titulo_3_es`, `texto_3_pt`, `texto_3_en`, `texto_3_es`, `link_3`, `imagem_4`, `titulo_4_pt`, `titulo_4_en`, `titulo_4_es`, `texto_4_pt`, `texto_4_en`, `texto_4_es`, `link_4`, `imagem_5`, `titulo_5_pt`, `titulo_5_en`, `titulo_5_es`, `texto_5_pt`, `texto_5_en`, `texto_5_es`, `link_5`, `imagem_6`, `titulo_6_pt`, `titulo_6_en`, `titulo_6_es`, `texto_6_pt`, `texto_6_en`, `texto_6_es`, `link_6`, `created_at`, `updated_at`) VALUES
(1, '3_20171110163038.jpg', 'NOSSOS NEGÓCIOS', '.', '.', '', '11_20171110163038.jpg', 'TALENCO CONSTRUTORA', '.', '.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', '.', '.', '#', '9_20171110163039.jpg', 'TALENCO SOLUÇÕES EM REFORMAS', '.', '.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', '.', '.', '#', '13_20171110163039.jpg', 'TALENCO IMÓVEIS', '.', '.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', '.', '.', '#', '19_20171110163039.jpg', 'TALENCO SOLUÇÕES EM REFORMAS', '.', '.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', '.', '.', '#', '23_20171110163039.jpg', 'TALENCO IMÓVEIS', '.', '.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', '.', '.', '#', NULL, '2017-11-10 16:31:51');

-- --------------------------------------------------------

--
-- Estrutura para tabela `configuracoes`
--

CREATE TABLE `configuracoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome_do_site` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_de_compartilhamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `analytics` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `configuracoes`
--

INSERT INTO `configuracoes` (`id`, `nome_do_site`, `title`, `description`, `keywords`, `imagem_de_compartilhamento`, `analytics`, `created_at`, `updated_at`) VALUES
(1, 'TalenCo', 'TalenCo', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `endereco`, `google_maps`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', '+55 13 3395-0005', 'Rua Esp&iacute;rito Santo, 182 - Canto do Forte<br />\r\n11700-190 - Praia Grande - SP&nbsp;', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3644.6488435985384!2d-46.409786885013766!3d-24.008174284462925!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce1daef198f683%3A0xcbb61a863b38c45c!2sR.+Esp%C3%ADrito+Santo%2C+182+-+Canto+do+Forte%2C+Praia+Grande+-+SP%2C+11700-190!5e0!3m2!1spt-BR!2sbr!4v1510330953504\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', NULL, '2017-11-10 16:22:37');

-- --------------------------------------------------------

--
-- Estrutura para tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2017_09_01_163723_create_configuracoes_table', 1),
('2017_11_09_184740_create_newsletter_table', 1),
('2017_11_09_191703_create_sobre_table', 1),
('2017_11_09_194637_create_cabecalhos_table', 1),
('2017_11_09_195259_create_banners_table', 1),
('2017_11_09_200551_create_chamadas_table', 1),
('2017_11_09_201647_create_unidades_table', 1),
('2017_11_09_205634_create_unidades_detalhes_table', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `sobre`
--

CREATE TABLE `sobre` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_es` text COLLATE utf8_unicode_ci NOT NULL,
  `missao_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `missao_en` text COLLATE utf8_unicode_ci NOT NULL,
  `missao_es` text COLLATE utf8_unicode_ci NOT NULL,
  `visao_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `visao_en` text COLLATE utf8_unicode_ci NOT NULL,
  `visao_es` text COLLATE utf8_unicode_ci NOT NULL,
  `valores_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `valores_en` text COLLATE utf8_unicode_ci NOT NULL,
  `valores_es` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `sobre`
--

INSERT INTO `sobre` (`id`, `ordem`, `slug`, `titulo_pt`, `titulo_en`, `titulo_es`, `texto_pt`, `texto_en`, `texto_es`, `missao_pt`, `missao_en`, `missao_es`, `visao_pt`, `visao_en`, `visao_es`, `valores_pt`, `valores_en`, `valores_es`, `created_at`, `updated_at`) VALUES
(1, 0, 'perfil-corporativo', 'Perfil Corporativo', 'Perfil Corporativo', 'Perfil Corporativo', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam venenatis quam luctus justo sodales, sed lacinia justo malesuada. Aliquam efficitur cursus erat, at tempor enim ultrices a. Nam odio mauris, sodales sit amet scelerisque a, mattis eu tortor.</p>\r\n\r\n<h2>Integer tempor sem sit amet nulla vulputate egestas.</h2>\r\n\r\n<ul>\r\n	<li>Etiam facilisis, sem ac posuere gravida, nisl lacus consectetur massa, at elementum ligula libero eu nisl.</li>\r\n	<li>Vestibulum auctor pulvinar metus vel accumsan.</li>\r\n	<li>Phasellus vel sem id libero auctor auctor. Sed scelerisque pellentesque libero quis viverra. Fusce molestie quis ante ac viverra. Nam nisl lorem, fermentum feugiat venenatis ac, pulvinar id magna. Sed at justo condimentum, lacinia lectus et, dignissim lectus.</li>\r\n</ul>\r\n', '<p>.</p>\r\n', '<p>.</p>\r\n', '', '', '', '', '', '', '', '', '', NULL, '2017-11-10 16:27:30'),
(2, 1, 'missao-visao-e-valores', 'Missão, Visão e Valores', 'Missão, Visão e Valores', 'Missão, Visão e Valores', '', '', '', '.', '.', '.', '.', '.', '.', '.', '.', '.', NULL, '2017-11-10 16:25:51'),
(3, 2, 'cultura-organizacional', 'Cultura Organizacional', 'Cultura Organizacional', 'Cultura Organizacional', '<p>.</p>\r\n', '<p>.</p>\r\n', '<p>.</p>\r\n', '', '', '', '', '', '', '', '', '', NULL, '2017-11-10 16:23:53'),
(4, 3, 'responsabilidade-social', 'Responsabilidade Social', 'Responsabilidade Social', 'Responsabilidade Social', '<p>.</p>\r\n', '<p>.</p>\r\n', '<p>.</p>\r\n', '', '', '', '', '', '', '', '', '', NULL, '2017-11-10 16:24:10');

-- --------------------------------------------------------

--
-- Estrutura para tabela `unidades`
--

CREATE TABLE `unidades` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_es` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `unidades`
--

INSERT INTO `unidades` (`id`, `ordem`, `slug`, `titulo_pt`, `titulo_en`, `titulo_es`, `texto_pt`, `texto_en`, `texto_es`, `created_at`, `updated_at`) VALUES
(1, 0, 'construcao-e-incorporacao', 'Construção E Incorporação', 'Construção E Incorporação', 'Construção E Incorporação', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam venenatis quam luctus justo sodales, sed lacinia justo malesuada. Aliquam efficitur cursus erat, at tempor enim ultrices a. Nam odio mauris, sodales sit amet scelerisque a, mattis eu tortor. Integer tempor sem sit amet nulla vulputate egestas. Etiam facilisis, sem ac posuere gravida, nisl lacus consectetur massa, at elementum ligula libero eu nisl. Vestibulum auctor pulvinar metus vel accumsan.</p>\r\n\r\n<p>Phasellus vel sem id libero auctor auctor. Sed scelerisque pellentesque libero quis viverra. Fusce molestie quis ante ac viverra. Nam nisl lorem, fermentum feugiat venenatis ac, pulvinar id magna. Sed at justo condimentum, lacinia lectus et, dignissim lectus.</p>\r\n', '<p>.</p>\r\n', '<p>.</p>\r\n', NULL, '2017-11-10 16:28:03'),
(2, 1, 'solucoes-em-reformas', 'Soluções Em Reformas', 'Soluções Em Reformas', 'Soluções Em Reformas', '<p>.</p>\r\n', '<p>.</p>\r\n', '<p>.</p>\r\n', NULL, '2017-11-10 16:26:24'),
(3, 2, 'negocios-imobiliarios', 'Negócios Imobiliários', 'Negócios Imobiliários', 'Negócios Imobiliários', '<p>.</p>\r\n', '<p>.</p>\r\n', '<p>.</p>\r\n', NULL, '2017-11-10 16:26:11');

-- --------------------------------------------------------

--
-- Estrutura para tabela `unidades_detalhes`
--

CREATE TABLE `unidades_detalhes` (
  `id` int(10) UNSIGNED NOT NULL,
  `unidade_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_es` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `unidades_detalhes`
--

INSERT INTO `unidades_detalhes` (`id`, `unidade_id`, `ordem`, `imagem`, `titulo_pt`, `texto_pt`, `titulo_en`, `texto_en`, `titulo_es`, `texto_es`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'ico-acompanhamento_20171110162947.png', 'Exemplo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam venenatis quam luctus justo sodales, sed lacinia justo malesuada. Aliquam efficitur cursus erat, at tempor enim ultrices a.', '.', '.', '.', '.', '2017-11-10 16:28:17', '2017-11-10 16:29:47');

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$eUnATjAdOCvkf86nwziQVO0DgSE4Zd.f5XhZlAi5gXqI5n/TOnfWG', '7N7DDhoza0bRsl4ZADy03jZjx3MyoeTVRuIaP4GuKrHVGicwIy9aZ3bcB1rB', NULL, '2017-11-10 16:19:00');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `cabecalhos`
--
ALTER TABLE `cabecalhos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `chamadas`
--
ALTER TABLE `chamadas`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `configuracoes`
--
ALTER TABLE `configuracoes`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `newsletter_email_unique` (`email`);

--
-- Índices de tabela `sobre`
--
ALTER TABLE `sobre`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `unidades`
--
ALTER TABLE `unidades`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `unidades_detalhes`
--
ALTER TABLE `unidades_detalhes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unidades_detalhes_unidade_id_foreign` (`unidade_id`);

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `cabecalhos`
--
ALTER TABLE `cabecalhos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `chamadas`
--
ALTER TABLE `chamadas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `configuracoes`
--
ALTER TABLE `configuracoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `sobre`
--
ALTER TABLE `sobre`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `unidades`
--
ALTER TABLE `unidades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `unidades_detalhes`
--
ALTER TABLE `unidades_detalhes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `unidades_detalhes`
--
ALTER TABLE `unidades_detalhes`
  ADD CONSTRAINT `unidades_detalhes_unidade_id_foreign` FOREIGN KEY (`unidade_id`) REFERENCES `unidades` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
